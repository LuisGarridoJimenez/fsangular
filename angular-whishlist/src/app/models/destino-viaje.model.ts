export class DestinoViaje {
    private selected: boolean;
    public servicios: string[];

    constructor(public nombre: string, public imagenUrl: string) {
        this.servicios = ['Piscina', 'Desayuno'];
     }

    isSelected(): boolean {
        console.log('isSlected');
        return this.selected;
    }

    setSelected(s: boolean): void {
        console.log('setSlected ' + s);
        this.selected = s;
    }
}
