import { DestinoViaje } from './destino-viaje.model';

export class DestinosApiClient {
    destinos: DestinoViaje[];

    constructor() {
        console.log('estoy en el constructor api-client');
       this.destinos = [];
    }

    add(d: DestinoViaje) {
        this.destinos.push(d);
        console.log(this.destinos);
    }

    getAll() {
        return this.destinos;
    }
}
